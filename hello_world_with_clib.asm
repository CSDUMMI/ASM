; nasm -felf64 hello_world_with_clib.asm && gcc hello_world_with_clib.o && ./a.out

global main
extern puts

section .text
main:

    mov rdi, message
    call puts
    ret

message:
    db "Hello, World", 10, 0
